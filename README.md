OLD CI

# List of stages for jobs, and their order of execution
stages:
  - build
  - test
  - deploy
  - update
  
sast:
  stage: test
include:
- template: Security/SAST.gitlab-ci.yml

build-docker:
  only:
    - dev
    - main
  stage: build
  image: docker:24.0.5
  services:
    - docker:24.0.5-dind
  before_script:
    - echo -n $DOCKER_HUB_TOKEN | docker login -u $DOCKER_HUB_USER --password-stdin docker.io 
  script:
    - docker build --tag $DOCKER_HUB_USER/$CI_PROJECT_NAME:$CI_COMMIT_REF_NAME --no-cache .
    - docker push docker.io/$DOCKER_HUB_USER/$CI_PROJECT_NAME:$CI_COMMIT_REF_NAME


# deploy-job:      # This job runs in the deploy stage.
#   stage: deploy  # It only runs when *both* jobs in the test stage complete successfully
#   before_script:
#     - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
#     - mkdir -p ~/.ssh
#     - eval $(ssh-agent -s)
#     - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    
#   script:
#     - echo "Deploying application..."
#     - chmod 600 $SSH_KEY_VPS
#     - ssh -p22 -i $SSH_KEY_VPS $SSH_USER@$SSH_IP "cd ./CUBE && sudo docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY && sudo docker compose pull && sudo docker compose up --force-recreate --detach && sudo docker image prune -f"
#     - echo "Application successfully deployed."

# deploy-terraform: # This job runs in the deploy stage.
#   stage: deploy
#   image:
#     name: hashicorp/terraform:1.4
#     entrypoint:
#       - '/usr/bin/env'
#       - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
#   before_script:
#     - cd terraform
#     - rm -rf .terraform
#     - terraform --version
#     - terraform init -backend-config="./env/$CI_COMMIT_REF_NAME-backend.tfvars" -reconfigure 
#   script:
#     - terraform plan -out="./$CI_PROJECT_NAME-$CI_COMMIT_REF_NAME.plan" -var-file="./env/$CI_COMMIT_REF_NAME.tfvars"
#     - terraform apply -var-file="./env/$CI_COMMIT_REF_NAME.tfvars" -auto-approve

# deploy-container:
#   image: mcr.microsoft.com/azure-cli
#   stage: update
#   script:
#     - az login --service-principal -u $ARM_CLIENT_ID -p $ARM_CLIENT_SECRET --tenant $ARM_TENANT_ID
#     - az container restart --resource-group rg-$CI_PROJECT_NAME-$CI_COMMIT_REF_NAME --name container-$CI_PROJECT_NAME-$CI_COMMIT_REF_NAME --no-wait
