git -C ./auth-api pull
docker build -t gabrieldela/auth-api ./auth-api
docker push gabrieldela/auth-api

git -C ./catalog-api pull
docker build -t gabrieldela/catalog-api ./catalog-api
docker push gabrieldela/catalog-api

git -C ./restaurant-api pull
docker build -t gabrieldela/restaurant-api ./restaurant-api
docker push gabrieldela/restaurant-api

git -C ./delivery-api pull
docker build -t gabrieldela/delivery-api ./delivery-api
docker push gabrieldela/delivery-api

git -C ./order-api pull
docker build -t gabrieldela/order-api ./order-api
docker push gabrieldela/order-api

git -C ./mailing-api pull
docker build -t gabrieldela/mailing-api ./mailing-api
docker push gabrieldela/mailing-api

git -C ./reporting-api pull
docker build -t gabrieldela/reporting-api ./reporting-api
docker push gabrieldela/reporting-api

kubectl create namespace auth-api
kubectl create namespace catalog-api
kubectl create namespace restaurant-api
kubectl create namespace delivery-api
kubectl create namespace order-api
kubectl create namespace mailing-api
kubectl create namespace reporting-api

kubectl delete -k ./kubernetes/dev
kubectl apply -k ./kubernetes/dev